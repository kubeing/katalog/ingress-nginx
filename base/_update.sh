#!/bin/bash
# If the first argument is set, use that ss Version, otherwise use the latest version starting with controller
# Might run on a mac, so use gnu sed etc
set -ex
VERSION=${1:-$(curl -s https://api.github.com/repos/kubernetes/ingress-nginx/releases | grep tag_name | grep controller | head -n 1 | cut -d '"' -f 4)}
echo $VERSION
kustomize build github.com/kubernetes/ingress-nginx/deploy/static/provider/cloud?ref=$VERSION > $(dirname $0)/ingress-nginx.yaml
